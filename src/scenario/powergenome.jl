"""
PowerGenome integrates data from the Public Data Liberation Project (PuDL) along with additional data from various other sources with the intention of automatic generation of a scenario from these sources.  The output of the tool is a set of input files that can be used in the GenX model.

A long-term goal could be to read these GenX input files directly from PowerGenome. 

In the meantime, we will work directly with the 'enhanced' PuDL database to gather input data.

In the simple capacity expansion model, I am using the notion of scenario that is set of three dataframes of weather, generators and load.  Should I include a separate dataframe for storage?   
"""

using TypedTables
using SQLite
pudl_filename="data/v2_1_pudl_powergenome.sqlite"
function initPuDLDB(filename::String)
    db = SQLite.DB(filename)
    return db
end
