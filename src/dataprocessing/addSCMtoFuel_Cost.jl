#=
Calculate the social cost of methane and add the value to the natural gas fuel cost. This is necessary because there is no built-in social cost of methane policy in switch.
=#

using DataFrames

datadir="data/expansion_data/"
fuel=CSV.read(datadir*"psco_fuel_costs_2021-2049.csv",DataFrame)
scm=CSV.read(datadir*"federal_scm_2021-2049.csv",DataFrame)
newfuel=deepcopy(fuel)
leak_rate=0.03
btu_lb=20551
lb_ton=2000
dollar_mmbtu=1e6/btu_lb/lb_ton*leak_rate .* scm[:,:SCM]
newfuel[newfuel[:,:fuel] .== "NaturalGas",:fuel_cost] .+= dollar_mmbtu
CSV.write(datadir*"psco_fuel_cost_switch.csv",newfuel)