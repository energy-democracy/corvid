using DataFrames
using CSV
using Query

data="data/expansion_data/"
psco_fuel=CSV.read(data*"psco_fuel_costs_2021-2049.csv",DataFrame)
fuel_cost_stack=DataFrames.stack(
    psco_fuel,[:Coal,:NaturalGas],[:Year],
    variable_name=:fuel, value_name=:fuel_cost)
fuel_cost = fuel_cost_stack |>
    @map({load_zone="PSCO",fuel=_.fuel,period=_.Year,fuel_cost=_.fuel_cost}) |>
    DataFrame
CSV.write(data*"psco_fuel_cost_switch.csv",fuel_cost) 
