using SQLite
using DataFrames
using SQLStrings
using CSV

nrel_db=SQLite.DB("data/v2_1_pudl_powergenome.sqlite")
scen_atb=CSV.read("src/scenario/nrel_atb_scenario.csv",DataFrame)

function makeStmt(db,p)
    s = "select * from technology_costs_nrelatb where atb_year = $(p.atb_year)
    AND technology = '$(p.technology)' AND (parameter = 'capex_mw' or parameter = 'fixed_o_m_mw') 
    AND cost_case= '$(p.cost_case)' AND tech_detail = '$(p.tech_detail)' AND financial_case = '$(p[:financial_case])'
    AND basis_year >= $(p.start_year) AND basis_year <= $(p.end_year)"
    return SQLite.Stmt(db,s)
end
function getData(stmt)
    m=DBInterface.execute(stmt) |> DataFrame
    return m
end
function makeGenBuildCost(db,p)
    stmt=makeStmt(db,p)
    df = getData(stmt)
    df_un = DataFrames.unstack(df,[:basis_year],:parameter,:parameter_value)
    insertcols!(df_un,1,:GENERATION_PROJECT => p.GENERATION_PROJECT)
    if p.GENERATION_PROJECT=="Battery"
        insertcols!(df_un,5,:gen_storage_energy_to_power_ratio => 4)
        insertcols!(df_un,6,:gen_storage_energy_overnight_cost => 0.0)
    else
        insertcols!(df_un,5,:gen_storage_energy_to_power_ratio => ".")
        insertcols!(df_un,6,:gen_storage_energy_overnight_cost => ".")
    end
    rename!(df_un,Dict(:basis_year => "build_year", 
        :capex_mw => "gen_overnight_cost",
        :fixed_o_m_mw => "gen_fixed_om"))
    return df_un
end
function makeAllGens(db,scen)
    allgens_array=[makeGenBuildCost(nrel_db,r) for r in eachrow(scen)]
    allgens=reduce(vcat,allgens_array)
    return allgens
end
CSV.write("data/expansion_data/nrelatb_gen_build_costs.csv",makeAllGens(nrel_db,scen_atb))