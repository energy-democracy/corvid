using DataFrames
using CSV
using Dates
using TimeZones
using InvertedIndices

data="data/expansion_data/"

renewablesdata="data/8years/"


files=readdir(renewablesdata)
wind_files=filter(x->occursin("cheyenne_wind",x),files)
solar_files=filter(x->occursin("alamosa_solar",x),files)

readfile(name)=CSV.read(renewablesdata*name,DataFrame,comment="#")

winds=map(readfile,wind_files)
solars=map(readfile,solar_files)


foldmerge(xs)=foldl(append!, xs)

wind=foldmerge(winds)
solar=foldmerge(solars)

rename!(wind,:electricity => :wind)
rename!(solar,:electricity => :solar)
windsun=innerjoin(wind,solar,on=:time,makeunique=true)
select!(windsun,Not(:local_time_1))
windsun[!,1:2]=DateTime.(replace.(windsun[!,1:2]," "=>"T"))

windsun[!,1]=ZonedDateTime.(windsun[!,1],tz"UTC")


function repeatdata(df,reps)
    result=df
    length=size(df)[1]
    period=(df[end,:time]-df[1,:time])+(df[2,:time]-df[1,:time])
    for i in 2:reps
        result=vcat(result,df)
    end
    for i in 0:size(result)[1]-1
        result[i+1,:time]=df[(i % length)+1,:time]+period*div(i,length)
    end
    return result
end
windsun[!,:time]=DateTime.(windsun[!,:time])

windsun[!,:time]=DateTime.(windsun[!,:time])
begin #totalll haack
    startdate=DateTime(2021)
    offset=startdate-windsun[1,:time]
    windsun[!,:time]=windsun[!,:time].+offset
end

windsun = repeatdata(windsun,5)
windsun=windsun[!,Not(:local_time)]

CSV.write(data*"reninja_windsun_2021-2065.csv",windsun)
