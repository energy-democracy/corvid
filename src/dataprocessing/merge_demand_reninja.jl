using CSV
include("time.jl")
data="data/expansion_data/"
demand=read_csv_utc(data*"psco_demand_2021-2049.csv")
filter!(x->x.demand>0,demand)
vcf=read_csv_utc(data*"reninja_windsun_2021-2065.csv")
hourly=innerjoin(demand,vcf,on=:time)
CSV.write(data*"psco_hourly_2021-2049.csv",hourly)