using CSV
using DataFrames

seba=CSV.read("data/expansion_data/nrelatb_gen_build_costs.csv",DataFrame)
nyears=15
solar_rate=.12 # 12% decline per year
solar_base=1.0e6 # $/MW
function deflate_cost(base,rate,nyears)
    base*(1-rate).^(0:(nyears-1))
end
solar_cost=deflate_cost(solar_base,solar_rate,nyears)
wind_rate=.055 # 5.5% decline per year
wind_base=1.5e6 # $/MW, RethinkX base cost is essentially NREL base costs
wind_cost = deflate_cost(wind_base,wind_rate,nyears)
bat_base=1.365e6
bat_rate=.15
bat_cost=deflate_cost(bat_base,bat_rate,nyears)
function modify_cost_column(data,tech,newdata)
    data[data.GENERATION_PROJECT .== tech,:gen_overnight_cost] .= newdata
end
modify_cost_column(seba,"Solar",solar_cost)
modify_cost_column(seba,"Wind",wind_cost)
modify_cost_column(seba,"Battery",bat_cost)
CSV.write("data/expansion_data/seba_gen_build_costs.csv",seba)
