# get list of categories for the PSCO balancing authority
# download time series by generator
# combine all time series into DataFrame
using HTTP
using JSON
# eia api series example
# http://api.eia.gov/series/?series_id=sssssss&api_key=YOUR_API_KEY_HERE[&num=][&out=xml|json]
# Category for PSCO Balancing Authority
# EIA key registered to Brad Venner 

psco = (category_id="3390164",
        category_name="PSCo",
        eia_api_key="833b6ef00697e35bb680a447d49d8a4d",
        download_directory="data/expansion_data/",
)
function makeCategoryURL(p)
    eia_api_category_root="http://api.eia.gov/category/"
    category_string="category_id="*p.category_id
    eia_category_url=eia_api_category_root*"?"*api_string*"&"*category_string
    return eia_category_url
end
function getCategoryData(p)
    category_filename=p.category_name*"_category.json"
    category_file = p.download_directory*category_filename
    url = makeCategoryURL(p)
    HTTP.download(url,category_file)
    category_data=JSON.parsefile(category_file)
    return category_data
end
# select series with UTC datetimes
function makeSeriesURL(series_id,p)
    eia_api_series_root="http://api.eia.gov/series/"
    url = eia_api_series_root*"?"*"series_id="*series_id*"&api_key="*p.eia_api_key*"&out=json"
    return url
end

function getSeriesURLs(category_data,p)
    gens_array=filter(d -> d["f"] == "H",category_data["category"]["childseries"])
    url_array=String[]
    for i in gens_array
        push!(url_array,makeSeriesURL(i["series_id"],p))
    end
    return url_array
end
