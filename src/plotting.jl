using Unitful
using SHA
using Colors
using Gadfly
using CSV
using DataFrames
using Permutations
include("time.jl")


const defaultcolorschemesdir="misc/colorschemes.csv"
ordergens = ["storage","coal","gas","geo","wind","solar"]
const gentech = sort(ordergens) 


ncolor(s::String)=HSV(parse(Color,s))
default=Dict{String,Color}([
    "solar" => ncolor("khaki1")
    "wind" => ncolor("seagreen1")
    "gas" => ncolor("blue")
    "geo" => ncolor("gold3")
    "coal" => ncolor("grey10")
    "storage" => ncolor("fuchsia")
])


function symbolify!(df::DataFrame,id=1)
    df[!,id]=map(Symbol,df[!,id])
    return df
end


#=


coal
wind
solar
storage
gas
=#

function occursincase(x::String,y::String)
    return occursin(lowercase(x),lowercase(y))
end







function permsort(df::DataFrame,id::Symbol,order::Permutation)
    sort!(df,id)
    gdf=groupby(df,[id])
    if length(order)==length(gdf)
        gdfblah=gdf[order.data]
        return vcat(gdfblah...)
    else
        throw("BoundsError:Permutation is wrong size")
    end
end






function listperm(a::Array)
    return Permutation(sortperm(a))'
end
const defaultorder = listperm(ordergens)
function makecolorschemes(a::Array)
    df=DataFrame(:gens => gentech)
    for i in a
        blah(x)=get(i,x,ncolor("black"))
        insertcols!(df,size(df)[2]+1,"default"=>blah.(df[!,1]))
    end
    st=size(df)[2]
    for i in 1:0
        si=string(i)
        insertcols!(df,i+st,Symbol("rcolor"*si)=>rcolor.(df[!,1],seed=si))
    end
    df=stack(df,names(df)[2:end])
    rename!(df,:variable =>:colorscheme)
    sort!(df,[:colorscheme,:gens])
    CSV.write(defaultcolorschemesdir,df)
    return df
end


function categorise(s::String)
    for gen in gentech
        if occursincase(gen,s)
            return gen
        end
    end
    function listmatch(l::Array{String})
        for i in l
            if occursincase(i,s)
                return true
            end
        end
        return false
    end
    if listmatch(["ccgs","ncgt","ccgt","ct"])
        return "gas"
    elseif listmatch(["batter","capacit"])
        return "storage"
    else
        return missing
    end
end



# rename!(df,[:GEN_BLD_YRS_1=>:gens,:GEN_BLD_YRS_2=>:time,:BuildGen=>:value])
function simplifygens(dfo::DataFrame)
    df=dfo
    df[!,:gens]=categorise.(df[!,:gens])
    dropmissing!(df)
    gdf=groupby(df,[:gens,:time])
    df=combine(gdf,:value=>sum=>:value)
    return df
end
hex2num(hs)=sum(256^(i-1)*hs[i] for i in 1:lastindex(hs))

function masterplotter(scendir;forceregen=false,colorschemes::String=defaultcolorschemesdir)
	scens = readdir(scendir)
	filter!(x-> !occursin(".txt",x),scens)
	function shouldplot(scen) 
		return in("outputs",readdir("$scendir/$scen")) && ( forceregen || !in("plots",readdir("$scendir/$scen")))
	end
	filter!(shouldplot,scens)
    scens = map(x-> scendir*"/"*x,scens)
    gencolors = CSV.read(colorschemes,DataFrame)
    gencolors[!,:value] = dparse.(gencolors[!,:value])
    order = defaultorder
    definedscenario(x)=plotscenario(x,gencolors,order)
    definedscenario.(scens)
    return "Success!"
end

function plotscenario(scendir,gencolors::DataFrame,genorder::Permutation)
    plotsdir=scendir*"/plots"
    rm(plotsdir,recursive=true)
    mkpath(plotsdir*"/data")
    #=
    Color Scheme bullshit hole
    =#

    # Generates the Generation built every Year Table
    builds=CSV.read(scendir*"/outputs/BuildGen.csv",DataFrame)
    rename!(builds,[:GEN_BLD_YRS_1=>:gens,:GEN_BLD_YRS_2=>:time,:BuildGen=>:value])
    builds=simplifygens(builds)

    # Generates Energy Created Each Year
    dispatch = CSV.read(scendir*"/outputs/dispatch_annual_summary.csv",DataFrame)
    energy=rename(dispatch,[:gen_tech => :gens,:period => :time,:Energy_GWh_typical_yr=>:value])
    energy=simplifygens(energy)
    
    # Generates Emissions created per year
    carbon=rename(dispatch,[:gen_tech => :gens,:period => :time,:DispatchEmissions_tCO2_per_typical_yr=>:value])
    carbon=simplifygens(carbon)


    for data in [(builds,"buildcapacity"),(energy,"yearlyenergy"),(carbon,"yearlyemissions")]
        simplifygens(data[1])
        genplots(data[1],data[2],gencolors,genorder,plotsdir)
    end
end


function genplots(df::DataFrame,name::String,colors::DataFrame,order::Permutation,dir)
    df=permsort(df,:gens,order)
    CSV.write(string(dir,"/data/",name,".csv"),df)
     for i in groupby(colors,:colorscheme)
        plotname=string(dir,"/",name,"-",i[1,2])
        pallete=(i[!,:value])[order]
        genplotmain(df,plotname,pallete)
    end
end


function genplotmain(df::DataFrame,dir::String,pallete::Array)
    img=SVG("$dir.svg",16cm,9cm)
    p=Gadfly.plot(df,x=:time,y=:value,color=:gens,Geom.bar(position=:stack),Scale.color_discrete_manual(pallete...))
    draw(img,p)
    return img
end

function dparse(s::String)
    return eval(Meta.parse(s))
end
    seed=""
function rcolor(x;seed::String="")
    hash=hex2num(sha256(string(x)*seed)[1:4])
    hue= hash % 360 
    return HSV(hue,1,1)
end




function Base.getindex(a::AbstractArray,p::Permutation)
    if length(a)!=length(p)
        throw("BoundsError")
    end
    return getindex(a,p.data)
end

#=
function funplotting(df,num)
    for i in 1:num
        display(rstacked(df,string(i)))
        sleep(.6)
    end
end

function dstacked(df)
    siz=size(df)
    results=[zeros(siz[1]).+ 2.0]
    for i in 1:siz[2]
        push!(results,last(results)+df[:,i])
    end
    # fks=results
    rainbow(i)=HSV(div(360,siz[2])*(i% siz[2]),1,1)
    blaah=plot(results[2],fill=(results[1],1.0,rainbow(1)),label=(names(df)[1]))
    for i in 2:siz[2]+1
        plot!(results[i],fill=(results[i-1],1.0,rainbow(i*7)),label=(names(df)[i-1]))
    end
    return blaah
end

scendir="/home/nicole/Public/caw/psco-cep-switch/scenarios/scm80/"

function plotswitchweek(scendir,timeslice::TimeSlice,color::GenColor=default)
    scendir=scendir
    hours=CSV.read(scendir*"inputs/timepoints.csv",DataFrame)
    hours[!,:timeseries]=TimeSlice.(hours[!,:timeseries])
    hours=hours[(hours[:,:timeseries] .== map(x->timeslice,1:size(hours)[1])) ,:]
    rename!(hours,[:timepoint_id=>:id])
    storage=CSV.read(scendir*"outputs/StateOfCharge.csv",DataFrame)
    storage=innerjoin(hours,storage,on=[:id => :STORAGE_GEN_TPS_2])
    storage=DataFrame(:timestamp =>hours[!,:timestamp],:storage=>statestorage[!,:StateOfCharge])
    insert!(storage,3,:delta=>delta(storage[!,:storage]))
    gens=CSV.read(scendir*"outputs/dispatch-wide.csv")
    gens=innerjoin(hours,gens,on=:timestamp)
    gens=gens[!,Not(:Battery)]
    output=innerjoin[gens,sta]

    return 
end

function delta(xs::Array{Float64})
    ds=zeros(length(xs))
    ds[1:end-1]=xs[2:end]-xs[1:end-1]
    return ds
end


function plotscenario(scendir)
    dispatchwide=CSV.read(scendir*"outputs/dispatch-wide.csv",DataFrame)
    dispatchwide=dispatchwide[:,Not(:Battery)]
    result=DataFrame(dispatchwide[1,:])
    for yr in pcyear(dispatchwide[1,:timestamp]):pcyear(dispatchwide[end,:timestamp])
        dispatchyear=filter(x->year(x.timestamp) == yr, dispatchwide)
        temp=dispatchyear[1,:]
        adjust=8760.0
        temp[Not(:timestamp)]=colmean(dispatchyear[!,Not(:timestamp)],adjust)
        result=vcat(result,DataFrame(temp))
    end
    return result[2:end,:]
end
testslice=TimeSlice("PC2021-02-7||PC2021-03-6==>dark")

function colmean(df::DataFrame,mult::Float64=1.0)
	x=df[1,:]
	for i in 1:length(x)
		x[i]=mult*mean(df[!,i])
	end
	return x
end

Base.length(x::TimeSlice)=1

function makewide(df::DataFrame; x, y, value)
    sort!(df,[x])
    results =DataFrame(x => unique(df[!,x]))
    for col in unique(df[!,y])
        result = filter(row -> row[y]==col,df)[!,[x,value]]
        rename!(result,value=>Symbol(col))
        results=outerjoin(results,result,on=x)
    end
    return results
end

function makelong(df::DataFrame;id)
    results=[]
    ids=df[!,id]
    raw=df[!,Not(id)]
    l(x)=repeat([names(raw)[x]],length(ids))
    for i in 1:size(raw)[2]
        push!(results,DataFrame(id=> ids, :name => l(i), :value => raw[!,i]))
    end
    return dropmissing(vcat(results...))
end

function stacked(df)
    
    # fks=results
    blaah=plot(results[2],fill=(results[1],1.0,rcolor(names(df)[1],seed)),label=(names(df)[1]))
    for i in 2:siz[2]+1
        plot!(results[i],fill=(results[i-1],1.0,rcolor(names(df)[i-1],seed)),label=(names(df)[i-1]))
    end
    return blaah
end
=#


