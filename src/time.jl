using Dates
using TimeZones
using StatsBase
using Query
using DataFrames
using Random
using Intervals
weekstart= Dates.Monday

function rsubset(set,size)
    if size>length(set)
        return set
    elseif  size>(1-1/ℯ)*length(set)
        return shuffle(set)[1:size]
    else
        result = [rand(set)]
        for i in 2:size
            sel=rand(set)
            while in(sel,result)
                sel=rand(set)
            end
            push!(result,sel)
        end
        return result
    end
end

function rinset(superset,subset,size) # returns a random set I where I is contained in the superset and the subset is contained in I
    filter!((x->!in(x,subset)),superset)
    adding=rsubset(superset,size-length(subset))
    return sort(append!(adding,subset))
end

function timeslices(tb,te,slices,timesize) # timesize value taken as the integer number of hours
#    tb,te = DateTime.(tb,te)
    timesize = Dates.Millisecond(Dates.Hour(timesize))
    totaltime=te-tb
    samples=sort(rsubset(0:div(totaltime,timesize),slices))
    sampletimes=(x -> ([x,x+1].*timesize).+tb).(samples)
    hourslist=map(substhours,sampletimes)
    hourset=cat(dims=1,(hourslist)...)
    return unique(hourset)
end

struct PCDate
    date::Date
end

function PCDate(year::Int,week::Int=1,weekday::Int=1)
    startdate=tonext(Date(year),Dates.Friday)-Day(4)
    return PCDate(startdate+Day(7*(week-1)+weekday-1))
end



Date(d::PCDate)=d.date
Base.convert(::Type{Date},d::PCDate)=d.date
Base.convert(::Type{PCDate},d::PCDate)=PCDate(d)


function PCyearweekday(d::Date)
    w = Week(d).value
    y, m = yearmonth(d)
        if w == 1 && m == 12
            y += 1
        elseif (w == 52 || w == 53) && m == 1
            y -= 1
        end
    return (y, w, dayofweek(d))
end

PCyearweekday(d::PCDate)=PCyearweekday(d.date)

year(d::PCDate)=PCyearweekday(d)[1]
week(d::PCDate)=PCyearweekday(d)[2]
day(d::PCDate)=PCyearweekday(d)[3]




function Base.:-(x::PCDate,y::PCDate)
    return x.date-y.date
end

function Base.:isless(x::PCDate,y::PCDate)
    return isless(x.date,y.date)
end

function Base.:+(x::PCDate,y::DatePeriod)
    return PCDate(x.date+y)
end
function Base.:-(x::PCDate,y::DatePeriod)
    return x+(-y)
end

function Base.string(x::PCDate)
    return string("PC",lpad(year(x),4,"0"),"-",lpad(week(x),2,"0"),"-",day(x))
end

Base.show(io::IO,d::PCDate)= print(io,string(d))


function Base.parse(::Type{PCDate},x::String)
    test=match(r"PC(\d{4})-(\d{2})-(\d{1})",x)
    if typeof(test)==Nothing
        throw("Invalid PCDate String")
    else
        return PCDate(parse.(Int,test.captures)...)
    end
end
PCDate(s::String)=parse(PCDate,s)

struct PCDateTime
    datetime::DateTime
end

PCDateTime(year::Int)=PCDateTime(DateTime(PCDate(year).date))
PCDateTime(year::Int,week::Int)=PCDateTime(DateTime(PCDate(year,week).date))
PCDateTime(year::Int,week::Int,weekday::Int)=PCDateTime(DateTime(PCDate(year,week,weekday).date))

PCDateTime(d::Date,t::Time=Time(0))=PCDateTime(PCDate(d),t)
DateTime(dt::PCDateTime)=dt.datetime
Base.convert(::Type{DateTime},x::PCDateTime)=x.datetime
Base.convert(::Type{PCDateTime},x::DateTime)=PCDateTime(x)

PCDate(dt::DateTime)=PCDate(Date(dt))
PCDate(dt::PCDateTime)=PCDate(dt.datetime)
PCDateTime(d::PCDate,t::Time=Time(0))=PCDateTime(DateTime(d.date,t))



function Base.:-(x::PCDateTime,y::PCDateTime)
    return x.datetime - y.datetime
end

function Base.:isless(x::PCDateTime,y::PCDateTime)
    return isless(x.datetime,y.datetime)
end

function Base.:+(x::PCDateTime,y::Period)
    return PCDateTime(x.datetime+y)
end
function Base.:-(x::PCDateTime,y::Period)
    return x+(-y)
end
function Dates.Time(x::PCDateTime)
    return Time(x.datetime)
end

function Base.string(x::PCDateTime)
    return string(PCDate(x),"T",Time(x))
end

Base.show(io::IO,d::PCDateTime)= print(io,string(d))

function Base.parse(::Type{PCDateTime},s::String)
    test=match(r"([^T]*)T([^T]*)",s)
    if typeof(test)==Nothing
        throw("Invalid PCDateTime String")
    else
        return PCDateTime(PCDate(string(test.captures[1])),Time(string(test.captures[2])))
    end
end
PCDateTime(s::String)=parse(PCDateTime,s)

function printlist(list)
    for i in list
        println(i)
    end
end


function weeksinyear(yr::Int)
    return week(PCDate(yr)-Day(1))
end

weeksinyear(d::PCDate)=weeksinyear(year(d))
weeksinyear(dt::PCDateTime)=weeksinyear(dt.date)

daysperPCyear(yr)=7*weeksinyear(yr)

year(dt::DateTime)=Year(dt).value

struct TimeSlice
    interval::Interval{DateTime,Closed,Closed}
    reason::Array{String}
end

function TimeSlice(db::DateTime,de::DateTime,reason::Array{String})
    return TimeSlice(Interval(db,de),reason)
end

function Base.in(dt::DateTime,ts::TimeSlice)
    return in(dt,ts.interval)
end


function tb(x::TimeSlice)
    return x.interval.first
end

function te(x::TimeSlice)
    return x.interval.last
end

function TimeSlice(db::Date,de::Date,reason::Array{String})
    return TimeSlice(DateTime(db),DateTime(de)+Second(86399),reason) # 24*60*60 (seconds in day) - 1
end
TimeSlice(db::PCDate,de::PCDate,reason::Array{String})=TimeSlice(db.date,de.date,reason)

Base.in(dt::Date,ts::TimeSlice)=in(dt,ts.interval)

Base.string(x::TimeSlice)=string(PCDate(x.interval.first),"||",PCDate(x.interval.last),"==>",mystring(x.reason))

Base.show(io::IO,d::TimeSlice)= print(io,string(d))
mystring(x::Array{String})=string(x[1],map(z->"-"*z,x[2:end])...)



function myparse(::Type{Array{String}},s::String)
    t=findall(r"-[^-]*","-"*s)
    extractstring(x)=getindex(s,first(x):last(x)-1)
    return map(extractstring,t)
end


function Base.parse(::Type{TimeSlice},s::String)
    test=match(r"([^\|]*)\|\|([^\=]*)\=\=\>(.*)",s)
    if test == Nothing
        throw("Invalid Timeslice String")
    else
        return TimeSlice(PCDate(string(test.captures[1])),PCDate(string(test.captures[2])),myparse(Array{String},string(test.captures[3])))
    end
end

TimeSlice(s::String)=parse(TimeSlice,s)


function Base.isless(t1::TimeSlice,t2::TimeSlice)
    if tb(t1)<tb(t2)
        return true
    else
        return false
    end
end


function PCyearday(year::Int,day::Int)
    week=(day-1)÷ 7 +1
    weekday=(day-1) % 7 + 1
    return PCDate(year,week,weekday)
end

struct PCWeek
    year::Int
    week::Int
    function PCWeek(yr::Int,wk::Int)
        if 9999 < yr > 1900
            throw("Invalid Year")
        end
        if 1>wk>53
            throw("Invalid Week: Week id not in Year")
        end
        new(yr,wk)
    end
end

function PCWeek(d::PCDate)
    return PCWeek(PCyearweekday(d)[1:2]...)
end

PCWeek(d::Date)=PCDate(d)
PCWeek(d::DateTime)=PCDate(d)
PCWeek(d::PCDateTime)=PCDate(d)



function filteryear(df::DataFrame,yr::Int64)
    df |>
    @filter(Dates.year(_.time) == yr) |>
    DataFrame
end

read_csv_utc(path)=CSV.read(path,DataFrame,dateformat=DateFormat("yyyy-mm-ddTHH:MM:SS.s"))

function Base.Missing(x::Integer)
    return convert(Array{Any,1},map((y->Missing()),1:x))
end



function randomdays(yr,daysperyear,setdays=[])
    sampleddays=rinset(collect(1:daysperPCyear(yr)),setdays,daysperyear)
    sampledids=PCyearday.(yr,sampleddays)
    return samp
    ledids
end

function pcyear(d::Date)
    return year(PCDate(d))
end
pcyear(dt::DateTime)=pcyear(Date(dt))



function timefilter!(df,timeseries::Array{TimeSlice,1})
    sort!(timeseries)
    ids=Missing(size(df)[1])
    if !in("timeseries",names(df))
        insertcols!(df,2,:timeseries=> ids)
    else
        df[!,:timeseries]=ids
    end
    b=1
    for timepoint in timeseries
        while df[b,:time]<tb(timepoint)
            b+=1
        end
        while try df[b,:time]<te(timepoint) catch BoundsError false end
            df[b,:timeseries]=string(timepoint)
            b+=1
        end
    end
    disallowmissing!(df)
    df=df[completecases(df),:]
    periods=pcyear.(df[!,:time])
    if !in("period",names(df))
        insertcols!(df,3,:period=> periods)
    else
        df[!,:period]=periods
    end
end




function generateslices(df::DataFrame)
    df=sort(unique(df,:date),:date)
    df[!,:date]=Date.(df[!,:date])
    function uniquereasons(xs::Array{Array{String}})
        return unique(vcat(xs...))
    end

    function recursive(xs::DataFrame,slices::Array{TimeSlice,1})
        i=1
        while try xs[i,:date]+Day(1)==xs[i+1,:date] catch BoundsError false end
            i+=1
        end
        if i == size(xs)[1]
            return push!(slices,TimeSlice(xs[1,:date],xs[end,:date],uniquereasons(xs[:,:reason])))
        else
            return recursive(xs[i+1:end,:],push!(slices,TimeSlice(xs[1,:date],xs[i,:date],uniquereasons(xs[1:i,:reason]))))
        end
    end
    return recursive(df,TimeSlice[])
end