using JuMP
using Clp
function calcVarCostsSimple(gens,n_years,discount_rate)
	# Input:
	#	gens: generators 
	#	n_years: number of years of decision period 
	#   discount_rate: global discount rate
	# Output:
	#	fixed_costs:  matrix of n_gens x n_years
	# Calculation:
	#   Discount fixed O&M costs from present year 'gens' table
	var_vec=gens[:,:VarOM]+gens[:,:HeatRate].*gens[:,:FuelCost]+gens[:,:HeatRate].*gens[:,:Emissions].*gens[:,:SCC]
	var_costs = var_vec .* calcDiscountVec(discount_rate, n_years)'
	return(var_costs)
end
function evalMultiyearModel(scen)
	Brown_MY_Model = Model(Clp.Optimizer)
	n_gens=nrow(scen.gens)
	scen.gens.Index = collect(1:n_gens)
	g_i = scen.gens.Index
	gens_wind = @from i in scen.gens begin
		@where i.Capacity == "Wind"
		@select i.Index
		@collect
	end
	n_wind=length(gens_wind)
	gens_solar = @from i in scen.gens begin
		@where i.Capacity == "Solar"
		@select i.Index
		@collect
	end
	n_solar=length(gens_solar)
	gens_store = @from i in scen.gens begin
		@where i.Capacity == "Storage"
		@select i.Index
		@collect
	end
	n_store=length(gens_store)
	df_dispatch=DataFrame(Capacity=["Storage","Dispatch"])
	gens_dispatch = @from i in scen.gens begin
		@join j in df_dispatch on i.Capacity equals j.Capacity
		@select i.Index
		@collect
	end
	gens_new = @from i in scen.gens begin
		@where i.Existing== "No"
		@select i.Index
		@collect
	end
	n_newgens=
	gens_old = @from i in scen.gens begin
		@where i.Existing== "Yes"
		@select i.Index
		@collect
	end
	# n_dispatch=length(gens_dispatch)
	hours_store=4.0 # this eventually needs to be be queried from the database
	roundtrip_store=0.85 # this eventually needs to be be queried from the database
	Demand = scen.dem.Demand
    n_hours=nrow(scen.dem)
	n_years=1
	NSECost=9000*calcDiscountVec(scen.discount_rate,n_years)
	wind_cap=scen.cap[:,:Wind]
	solar_cap=scen.cap[:,:Solar]
	FixedCost=calcFixedCostsSimple(scen.gens,n_years+1,scen.discount_rate)
	VarCost=calcVarCostsSimple(scen.gens,n_years+1,scen.discount_rate)
	# variable structure
	## STATECAP, FLOWCAP: matrix, n_gens x n_years
	##   Capacity decisions are made on a year-to-year basis
	##   Decisions are made in FLOWCAP and accumulated in STATECAP
	## GEN: 3-D array, n_gens x n_years x n_hours
	##   Generation decisions are made on an hour-by-hour basis
	##   Generation cannot exceed the capacity available at the start of the year
	## CHARGE, STORE, WINDCURTAIL, SOLCURTAIL:  n_units x n_hours x n_years 
	##   Generator-specific decisions, conceptually sub-components of generation
	## NSE: n_hours x n_years
	##   Non-served capacity at each hourly time interval
	## TODO => RET: n_gens x n_years
	##   0-1 matrix indicating retirement status; retired generators have no fixed OM charges 
	@variables(Brown_MY_Model, begin
	    STATECAP[1:n_gens, 1:(n_years+1)] >= 0      # Generating capacity built (MW)
		NEWCAP[1:n_newgens, 1:n_years] 
	    GEN[1:n_gens, 1:n_hours, 1:n_years] >= 0  # Generation in each hour (MWh)
	    NSE[1:n_hours, 1:n_years] >= 0            # Non-served energy in each hour (MWh)
        CHARGE[1:n_store, 1:n_hours, 1:n_years] >= 0
        STORE[1:n_store, 1:n_hours,1:n_years] >= 0
		WINDCURTAIL[1:n_wind, 1:n_hours, 1:n_years] >=0
		SOLCURTAIL[1:n_solar, 1:n_hours, 1:n_years] >=0
	end)
	@constraints(Brown_MY_Model, begin
		# cWindCap & cSolarCap could be combined into a 4-D array
	    cWindCap[g in 1:n_wind, h in 1:n_hours, y in 1:n_years], 
			GEN[gens_wind[g],h,y] + WINDCURTAIL[g,h,y] == STATECAP[gens_wind[g],y]*wind_cap[h,y]
		cSolarCap[g in 1:n_solar, h in 1:n_hours, y in 1:n_years], 
			GEN[gens_solar[g],h,y] + SOLCURTAIL[g,h,y] == STATECAP[gens_solar[g],y]*solar_cap[h,y]
		# Capacity is a dynamic system with state & flow 
		cDispatchCap[g in gens_dispatch, h in 1:n_hours, y in 1:n_years],
			GEN[g,h,y] <=  STATECAP[g,y]
		dStateFlowCap[g in 1:n_gens, y in 1:n_years],
			STATECAP[g,y+1] == FLOWCAP[g,y] + STATECAP[g,y]
		cCapacityInit[g in 1:n_gens], 
			STATECAP[g,0] == scen.gens[g,:ExistingCapacity]   
		# Zero cap	
        cBatEnergy[s in 1:n_store, h in 1:n_hours, y in 1:n_years], 
			STORE[s,h,y] <= hours_store * STATECAP[gens_store[s],y]
	#	Temoa uses a 'timeslice' dimension; battery state must return to starting value at end of timeslice
	#	For now, carry over battery state from last timeslice
		cBatCharge[s in 1:n_store, h in 2:n_hours, y in 1:n_years], 
			STORE[s,h,y] == STORE[s,h,y]-GEN[gens_store[s],h,y] + roundtrip_store*CHARGE[s,h,y]
		cInitBatCharge[s in 1:n_store], STORE[s,1,1]==0
		cDemandBalance[h in 1:n_hours, y in 1:n_years], 
			sum( GEN[g,h,y] for g in g_i) + NSE[h,y] == 
				Demand[h,y] + sum(CHARGE[s,h,y] for s in 1:n_store)
	end)
	# gens_year: DataFrame with Generator, Years, Parameter, Value, Unit
	# dem: DataFrame with Index, Year, Demand
    # gens[:Generator == g && :Year == dem[h == :Index, :Year] && Parameter= :VarCost,:Value]
	# Or:
	# FixedCost, VarCost:  matrix of years X gen (uses matrix algebra - would this be faster?)
	# Solve for net present value when matricies are formed
	@objective(Brown_MY_Model, Min, begin
    	sum((FixedCost[g,y][1]*STATECAP[g,y] for y in 1:(n_years+1)) for g in 1:n_gens)
	#	+ sum(NSE*NSECost) 
    #    + sum(sum(VarCost'*GEN[:,h,:]) for h in 1:n_hours)
	end)
	JuMP.optimize!(Brown_MY_Model)
	genv=value.(GEN).data
	capv=value.(CAP).data
	chargev=value.(CHARGE).data
	storev=value.(STORE).data
	gen_df=DataFrame(Tables.table(genv',header=scen.gens.G))
	charge_df=DataFrame(Tables.table(chargev',header=scen.gens.G[gens_store].*"_charge"))
	store_df=DataFrame(Tables.table(storev',header=scen.gens.G[gens_store].*"_state"))
	return((gen_df,charge_df,store_df))
#   return(Brown_MY_Model)
end

#=
function variableSCC(SCCs,generators,cap,demand)
    function evalSCCBrown(SCC)
        generatorsSCC=SetSCC(SCC,generators)
		scen = (gens=generatorsSCC,cap=cap,dem=demand)
        return evalBrownfieldModel(scen)
    end
    return pmap(SCCs,evalSCCBrown)
end
=#