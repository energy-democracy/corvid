using Base: DATAROOTDIR, GIT_VERSION_INFO

using DataFrames
using CSV
using Query
using Tables
using Statistics
using Dates

include("../math.jl")

include("../time.jl")
# include("../dataprocessing/dataclean.jl")
"""
	Find generation for each hour of Demand
	Use multiple years
	Use time slices within years
	Find unique index for each hour in Demand
	Call this H
	Generator financial data by year
"""

"ToDo:  Recalculate solar & wind for selected weeks in 8 year period"
# babble = "~/Documents/energy-democracy/corvid/"
# include(babble*"src/model/time.jl")
function calcCRF(interest_rate, n_period)
	FV=(1+interest_rate)^n_period
	CRF = interest_rate * FV/(FV-1)
	return CRF
end
function calcDiscountVec(discount_rate,n_years)
	return (1-discount_rate).^(collect(0:(n_years-1)))
end
function calcFixedCostsSimple(gens,n_years,discount_rate)
	# Input:
	#	gens: generators 
	#	n_years: number of years of decision period 
	#   discount_rate: global discount rate
	# Output:
	#	fixed_costs:  matrix of n_gens x n_years
	# Calculation:
	#   Discount fixed O&M costs from present year 'gens' table
	fixed_om_vec= gens[:,:FixedOM]
	cap_vec = gens[:,:Capex]
	wacc_vec = gens[:,:WACC]
	n_periods=gens[:,:AssetLife]
	fixed_vec= fixed_om_vec + cap_vec.*calcCRF.(wacc_vec,n_periods)
	fixed_costs = fixed_vec .* calcDiscountVec(discount_rate, n_years)'
	return(fixed_costs)
end

function build_switch_inputs_basic(scen)
	# Single investment period, sampled weeks in hourly
	n_hourly=nrow(scen["hourly"])
	last_hour=scen["hourly"][n_hourly,:time]
	last_year=Dates.year(last_hour)
	scen["hourly"][!,:TIMEPOINT]=collect(1:n_hourly)
	out=Dict()
	push!(out,"periods"=> scen["hourly"] |>
		@map({INVESTMENT_PERIOD=_.period,
			period_start=_.period,
			period_end=_.period+1}) |>
		@unique() |>
		DataFrame)
	base_period=out["periods"][1,:period_start]
	push!(out,"non_fuel_energy_sources" => DataFrame(energy_source=["Wind","Solar","Geothermal","Electricity"]))
	push!(out,"financials"=> DataFrame(
		base_financial_year=base_period,
		discount_rate=scen["discount_rate"],
		interest_rate=scen["interest_rate"]
		)
	)
	push!(out,"fuel_cost" => scen["fuel_cost"])
	push!(out,"fuels" => scen["gens"] |>
		@filter(_.Fuel == "Coal" || _.Fuel == "NaturalGas") |>
		@groupby(_.Fuel) |>
		@map({fuel=key(_),co2_intensity=mean(_.Emissions),upstream_co2_intensity=0.0}) |>
		DataFrame)
	push!(out,"gen_build_predetermined" => scen["predetermined"])
	push!(out,"generation_projects_info" => scen["gens"] |>
		@map({
			GENERATION_PROJECT=_.G,
			gen_tech=_.Description,
			gen_load_zone="PSCO",
			gen_connect_cost_per_mw=0.0,
			gen_capacity_limit_mw=".",
			gen_full_load_heat_rate= _.Fuel == "Coal" || _.Fuel == "NaturalGas" ? string.(_.HeatRate) : ".",
			gen_variable_om=_.VarOM,
			gen_max_age=_.AssetLife,
			gen_min_build_capacity=".",
			gen_scheduled_outage_rate=".",
			gen_forced_outage_rate=".",
			gen_is_variable =_.Capacity == "Wind" || _.Capacity == "Solar" ? 1 : 0,
#			unrampables=["Supercritical Coal","Natural gas CT"]
			gen_is_baseload = _.Description =="Supercritical Coal" ? 1 : 0,
			gen_is_cogen = ".",
			gen_energy_source = _.Fuel,
			gen_unit_size = ".",
			gen_ccs_capture_efficiency = ".",
			gen_ccs_energy_load = ".",
			gen_storage_efficiency = _.G == "Battery" ? 0.85 : ".",
			gen_store_to_release_ratio = "."
		}) |>
		DataFrame)
	push!(out, "load_zones"=> DataFrame(LOAD_ZONE="PSCO",dbid=1,						existing_local_td=9000.0,
			local_td_annual_cost_per_mw=0.0))
	push!(out,"loads" => scen["hourly"] |>
		@map({LOAD_ZONE="PSCO",_.TIMEPOINT,zone_demand_mw=_.demand}) |>
		DataFrame)
	push!(out,"timepoints" => scen["hourly"] |>
		@map({timepoint_id=_.TIMEPOINT,
			timestamp=string(_.time),
			timeseries=_.timeseries}) |>
			DataFrame)
	timeseries = scen["hourly"] |> @groupby(_.timeseries) |>
        @map({TIMESERIES=key(_),
                ts_period=unique(_.period)[1],
                ts_duration_of_tp=1,
                ts_num_tps=length(_)}) |>
        DataFrame
	period_length = timeseries |> @groupby(_.ts_period) |>
		@map({ts_period=key(_),ts_scale_to_period=365*24/sum(_.ts_num_tps)}) |>
		DataFrame
	push!(out,"timeseries" => innerjoin(timeseries,period_length,on=:ts_period))
#=
	push!(out,"timeseries" => ts_period |>
		@map({TIMESERIES=_.TIMESERIES,
			ts_period=_.ts_period,
			ts_duration_of_tp=_.ts_duration_of_tp,
			ts_num_tps=_.ts_num_tps,
			ts_scale_to_period=_.n_(365*24)/_.n_hours_period}) |> 
		DataFrame)
=#
	windcf = scen["hourly"] |>
		@map({GENERATION_PROJECT="Wind",
		timepoint=_.TIMEPOINT,
		gen_max_capacity=_.wind}) |>
		DataFrame
	solarcf = scen["hourly"] |>
		@map({GENERATION_PROJECT="Solar",
		timepoint=_.TIMEPOINT,
		gen_max_capacity=_.solar}) |>
		DataFrame
	push!(out,"variable_capacity_factors" => vcat(windcf, solarcf))
	push!(out,"carbon_policies" => scen["scc"] |>
		@map({PERIOD=_.Year,carbon_cap_tco2_per_yr=".",carbon_cost_dollar_per_tco2=_.SCC}) |>
		DataFrame)
	push!(out,"gen_build_costs"=>scen["build_cost"])
	return(out)
end

function write_switch_input(switch_dict::Dict,switch_dir::String)
	for i in keys(switch_dict)
		switch_dict[i] |> CSV.write(switch_dir*i*".csv")
	end
end


function makecoolsample(daysperyear::Integer,hourly::DataFrame)
	timeslices=TimeSlice[]
	for yr in year(hourly[1,:time]):year(hourly[end,:time])
		year=subset(hourly,:time => (xs -> pcyear.(xs) .== yr))
		year=dailyaverage(year)
		days=DataFrame("date"=>PCWeek[],"reason"=>Array{String}[])
		avdemand = weekaverage(year[!,:demand])
		avsolar =  weekaverage(year[!,:solar])
		avwind =  weekaverage(year[!,:wind])
		avrenew = (avsolar+avwind)./2
		hot = findmax(avdemand)
		dark = findmin(avsolar)
		calm = findmin(avwind)
		darkcalm = findmin(avrenew)
		maketuple(x,y)=(x,y)
		for add in [("hot",hot[2]),("darkcalm",darkcalm[2]),("calm",calm[2]),("dark",dark[2])]
			tmpweek=DataFrame("date"=>PCyearday.(yr,collect(0:6).+add[2]),"reason" .=> add[1])
			tmpweek[!,:reason]=map(x->String[x],tmpweek[!,:reason])
			days=vcat(days,tmpweek)
		end
		while size(days)[1]<daysperyear
			r=rand(1:daysperPCyear(yr)-6)
			tmpdaysblah=PCyearday.(yr,collect(0:6).+r)
			if intersect(tmpdaysblah,days[!,:date])==[]
				tmpweek=DataFrame("date"=>tmpdaysblah,"reason" .=> "random")
				tmpweek[!,:reason]=map(x->String[x],tmpweek[!,:reason])
				days=vcat(days,tmpweek)
			end
		end
		append!(timeslices,generateslices(days))
	end
	return timeslices
end


function weekaverage(xs)
	return (xs[1:end-6]+xs[2:end-5]+xs[3:end-4]+xs[4:end-3]+xs[5:end-2]+xs[6:end-1]+xs[7:end]) ./ 7
end



function dailyaverage(input::DataFrame)
	df=input
	df[!,:time]=Date.(df[!,:time])
	result = unique(df,:time)
	for i in 1:size(result)[1]
		date = result[i,:time]
		day=innerjoin(df,DataFrame(:time =>[date]),on=:time)
		result[i,Not(:time)]=colmean(day[!,Not(:time)])
	end
	return result
end

function colmean(df::DataFrame)
	x=df[1,:]
	for i in 1:length(x)
		x[i]=mean(df[!,i])
	end
	return x
end
begin
	brad_switch_dir="/Users/bradvenner/Documents/energy-democracy/psco-cep-switch/scenarios/"
	nicole_switch_dir="/home/nicole/Public/caw/psco-cep-switch/scenarios/"
	datadir = "data/expansion_data/"
	function createscenarios(scenarios,datadir,switchdir;sameslices::Bool=false)
		defaultvalues=scenarios[1,:]
		if sameslices
			hourly=CSV.read(datadir*scenarios[1,:hourly],DataFrame)
			sample=makecoolsample(scenarios[1,:slices],hourly)
			hourly=timefilter!(hourly,sample)
		end
		for i in 1:size(scenarios)[1]
			sc = scenarios[i,:]
			for k in 1:length(sc)
				if ismissing(sc[k]) 
					sc[k]=defaultvalues[k]
				end
			end
			scc=CSV.read(datadir*sc[:scc],DataFrame)
			scc[:,:SCC]=scc[:,:SCC].*sc[:sccmult]
			if !sameslices
				hourly=CSV.read(datadir*sc[:hourly],DataFrame)
				sample=makecoolsample(sc[:slices],hourly)
				hourly=timefilter!(hourly,sample)
			end
			fuel_cost=CSV.read(datadir*sc[:fuel],DataFrame)
			gen_build=CSV.read(datadir*sc[:build],DataFrame)
			gen_info=CSV.read(datadir*sc[:gens],DataFrame)
			predetermined=CSV.read(datadir*"psco_predetermined.csv",DataFrame)
			scendict = Dict("gens" => gen_info,
				"hourly" => hourly,
				"discount_rate"=>sc[:discount],
				"interest_rate"=>sc[:interest],
				"scc"=>scc,
				"fuel_cost"=>fuel_cost,
				"build_cost"=>gen_build,
				"predetermined"=>predetermined)
			m=build_switch_inputs_basic(scendict)
			inputsdir=switchdir*sc[:name]*"/inputs/"
			mkpath(inputsdir)
			write(inputsdir*"switch_inputs_version.txt","2.0.5")
			write_switch_input(m,inputsdir)
			println(string("Average Days Per Year for ",sc[:name]," is: ",size(hourly)[1]/(24*(pcyear(hourly[end,:time])-pcyear(hourly[1,:time])))))
		end
	end
	scenarios=CSV.read("src/model/scenarios.csv",DataFrame)
#	preformance=CSV.read("src/model/preformancetesting.csv",DataFrame)
	function executemodels(switchdir,forceregen=false)
		path=pwd()
		models = readdir(switchdir)
		filter!(x-> !occursin(".",x),models)
		function shouldrun(model) 
			if !in("outputs",readdir(switchdir*model)) || forceregen
				return true
			else
				return length(readdir(switchdir*model*"/outputs"))==0 
			end
		end
		filter!(shouldrun,models)
		function commandgen(model)
			return "conda activate switch2 ; switch solve --inputs-dir $model/inputs --outputs-dir $model/outputs --log-run --logs-dir $model/logs"
		end
		runmodels= commandgen.(models)
		return runmodels
	end
end