# Previous code - commented out so that I don't accidentally start editing it
begin
	Renewable_Model = Model(Clp.Optimizer)
	@variables(Renewable_Model, begin
	        CAP[g in Gr] >=0          # Generating capacity built (MW)
	        GEN[g in Gr, h in H] >= 0 # Generation in each hour (MWh)
	        NSE[h in H] >= 0         # Non-served energy in each hour (MWh)
	end)
	@constraints(Renewable_Model,
	begin
	    cDemandBalance[h in H], sum(GEN[g,h] for g in Gr) + NSE[h] == demand.Demand[h]
	    cCapacity[g in Gr, h in H], GEN[g,h] <= CAP[g]*cf[h,Symbol(g)]
	end)
	@objective(Renewable_Model, Min,
    sum(generators[generators.G.==g,:FixedCost][1]*CAP[g] + 
        sum(generators[generators.G.==g,:VarCost][1]*GEN[g,h] for h in H) for g in Gr) + 
    sum(NSECost*NSE[h] for h in H) 
)
end

function evalBrownfieldModel(scen)7
	Brown_Model = Model(Clp.Optimizer)
	n_gens=nrow(scen.gens)
	scen.gens.Index = collect(1:n_gens)
	g_i = scen.gens.Index
	gens_wind = @from i in scen.gens begin
		@where i.Capacity == "Wind"
		@select i.Index
		@collect
	end 
	gens_solar = @from i in scen.gens begin
		@where i.Capacity == "Solar"
		@select i.Index
		@collect
	end 
	gens_old = @from i in scen.gens begin
		@where i.ExistingCapacity>0
		@select i.Index
		@collect
	end
	gens_store = @from i in scen.gens begin
		@where i.Capacity == "Storage"
		@select i.Index
		@collect
	end
	df_dispatch=DataFrame(Capacity=["Storage","Dispatch"])
	gens_dispatch = @from i in scen.gens begin
		@join j in df_dispatch on i.Capacity equals j.Capacity
		@select i.Index
		@collect
	end
	n_store=length(gens_store)
	store_i = collect(1:n_store)
	hours_store=4.0 # this eventually needs to be be queried from the database
	roundtrip_store=0.85 # this eventu7ally needs to be be queried from the database
	Demand = scen.dem.Demand
    H = scen.dem.Hour
	NSECost=9000
	@variables(Brown_Model, begin
	    CAP[g in g_i] >=0          # Generating capacity built (MW)
	    GEN[g in g_i, h in H] >= 0 # Generation in each hour (MWh)
	    NSE[h in H] >= 0            # Non-served energy in each hour (MWh)
        CHARGE[s in store_i, h in H] >= 0
        STORE[s in store_i, h in H] >= 0
		STRCAP >= 0
	end)
	@constraints(Brown_Model, begin
	    cWindCap[g in gens_wind, h in H], GEN[g,h] <= CAP[g]*scen.cap[h,y]
		cSolarCap[g in gens_solar, h in H], GEN[g,h] <= CAP[g]*scen.cap[h,:Solar]
		cDispatchCap[g in gens_dispatch, h in H], GEN[g,h] <= CAP[g]
		cRetirement[g in gens_old], CAP[g] <= scen.gens[g,:ExistingCapacity]
        cBatEnergy[s in store_i, h in H], STORE[s,h] <= hours_store * CAP[gens_store[s]]
		cBatCharge[s in store_i, h in H[2:end]], STORE[s,h] == STORE[s,h-1]-GEN[gens_store[s],h] + roundtrip_store*CHARGE[s,h]
		cInitBatCharge[s in store_i], STORE[s,1]==0
		cDemandBalance[h in H], sum(GEN[g,h] for g in g_i) + NSE[h] == Demand[h] + sum(CHARGE[s,h] for s in store_i)
	end)
	@objective(Brown_Model, Min, begin
    	sum(scen.gens[g,:FixedCost][1]*CAP[g] + 
        	sum(scen.gens[g,:VarCost][1]*GEN[g,h] for h in H) for g in g_i) +
    	sum(NSECost*NSE[h] for h in H)
	end)
	JuMP.optimize!(Brown_Model)
	genv=value.(GEN).data
#	capv=value.(CAP).data
	chargev=value.(CHARGE).data
	storev=value.(STORE).data
	gen_df=DataFrame(Tables.table(genv',header=scen.gens.G))
	charge_df=DataFrame(Tables.table(chargev',header=scen.gens.G[gens_store].*"_charge"))
	store_df=DataFrame(Tables.table(storev',header=scen.gens.G[gens_store].*"_state"))
	return((gen_df,charge_df,store_df))
#	return(Brown_Model)
end
