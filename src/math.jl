function decimal(x)
	return x - floor(x)
end

# defined in the julia code as gcdx(a,b)
#=
function extendedgcd(a,b)
    init= [a 1 0 
           b 0 1]
    function recursive(x)
      if x[2,1] == 0 return x
      else
        q = x[1,1] ÷ x[2,1]
        result = [x[1,n]-q*x[2,n] for n=(1:3)']
        return recursive([x[2:2,:];result])
      end
    end
    return recursive(init)[1,:]
end
=#
function modinv(a,c) # finds the modular inverse of a mod c
    return mod(gcdx(a,c)[2],c)
end
function completesystem(a,b,c) # ax = b mod (c)
	k = gcd(a,c)
	p = gcd(b,c)
	if k == 1
		return [mod(modinv(a,c)*b,c)]
	elseif mod(p,k) != 0
		return []
	else
		blah = [a,b,c].÷k
		i = completesystem(blah...)[1]
		return i:c1:(c-1)
	end
end

function chineseprocessing(xs)
  function chineseprocessing_main(x)
    a,b,c = x
    sol = completesystem(a,b,c)
    if length(sol)==0
      return "One equation has no solutions"
    else
      return [sol[1],div(c,gcd(a,c))]
    end
  end
  return map(chineseprocessing_main,xs)
end


function chinesesol(xs) # solves the 2d case of the following problem
  n1, n2 = xs[1][2], xs[2][2]
  a1, a2 = xs[1][1], xs[2][1]
  exgcd = gcdx(n1,n2)
#  println(string(n1)*"\n"*string(n2)*"\n"*string(exgcd))
  if exgcd[1] != 1
    return "BAD SO SAD: not coprime numbers"
  else
    m1,m2 = exgcd[2],exgcd[3]
#    println(a1*m2*n2+a2*m1*n1)
    return mod(a1*m2*n2+a2*m1*n1,n1*n2)
  end
end

function chinese(xs) # takes in a list of 2 element lists [a_i,b_i] where $x=a_i \mod b_i$
  if typeof(xs[1])==String
    return xs[1]
  elseif length(xs) == 2
    return chinesesol(xs)
  else
    return chinese(push!(xs[3:end],[chinesesol(xs[1:2]),xs[1][2]*xs[2][2]]))
  end
end

function printextendedgcd(a,b)
  init= [a 1 0 
         b 0 1]
  function recursive(x)
    if x[2,1] == 0 return x
    else
      q = x[1,1] ÷ x[2,1]
      result = [x[1,n]-q*x[2,n] for n=(1:3)']
      for n in 1:3
        print(string(x[1,n])*" - "*string(q)*" \\cdot "*string(x[2,n])*" = "*string(result[n])*" & ")
      end
      println("\\\\")
      return recursive([x[2:2,:];result])
    end
  end
  return recursive(init)[1,:]
end

function legendre(p,q)
  function recursive(p,q)
    if p ==1
      return 1
    elseif mod(p,2) == 0
      return (-1)^((q^2-1)>>3)*legendre(p ÷ 2, q)
    else
      return (-1)^(((p-1)*(q-1))>>2) * legendre(mod(q,p),p)
    end
  end
  if gcd(p,q) != 1
    return 0
  else
    return recursive(mod(p,q),q)
  end
end

function modquad(p,q)
  p = mod(p,q)
  for i in 0:(q>>1)
    if i^2 % q == p
      return [i,q-i]
    end
  end
  return []
end

function modquadratic(a,b,c,n) # form ax^2+bx+c =0 mod n
  return  ((modquad((b^2-4*a*c),n).-b).* modinv(2*a,n)).% n
end

function fmexp(a,b,n) 
  mm(p,q)=mod(p*q,n)
  bbinary=digits(b,base=2)
  len=length(bbinary)
  list = [a]
  for i in 2:len
    push!(list,mm(list[end],list[end]))
  end
  result=[1-bbinary[i]+bbinary[i]*list[i] for i in 1:len]
  return foldl(mm,result)
end


function primality(n)
  accuracy=20 # How many iterations this will be accurate having only 1 error out of 4^n or 8^n iterations
  
  if n % 2 == 0
    return false
  else
    s=trailing_zeros(n-1)
    d=n>>s
    status=true
    for i in 1:accuracy
      a=rand(2:n-2)
      x = fmexp(a,d,n)
      test = ( x == n-1 || x == 1)
      for k in 1:s
        x=mod(x^2,n)
        test |=  (x == n-1)
      end
      status &= test
    end
    return status
  end
end

function primelist(a,b)
  if a<3
    result = [2,3]
    for i in 5:2:b
      if primality(i)
        append!(result,i)
      end
    end
  else
    result=[]
    for i in ((a>>1)<<1+1:2:b)
      if primality(i)
        append!(result,i)
      end
    end
  end
  return result
end

function fft(xs,w)
  return "blah"
end

primelist1k=primelist(0,1000)

function listfactor(n)
  if primality(n)
    return [n]
  else
    function trial(ns)
      if ns[1]==1
        return ns[2:end]
      end
      for i in primelist1k
        if ns[1] % i == 0
          ns=trial(append!([ns[1] ÷ i,i],ns[2:end]))
        end
      end
      return sort(ns)
    end
    ns=trial([n])
    if ns[end]<1000^2
      return ns
    else
      return "Too big will write a faster algorithm later"
    end
  end
end

function factor(n)
  factors=listfactor(n)
  if typeof(factors)==String
    return "Error"
  else
    result=[]
    while length(factors) != 0
      i=factors[1]
      places=findall(x -> (x==i),factors)
      push!(result,(i,length(places)))
      deleteat!(factors,places)
    end
    return result
  end
end

function totient(n)
  factors=factor(n)
  tot = x -> (x[1]-1)*x[1]^(x[2]-1)
  return prod(map(tot,factors))
end

 