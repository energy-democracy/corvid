using DataFrames
using CSV
using Dates

stripyear(time)=Int(Year(time)/Year(1))

function byear(df)
    df[!,:year]=stripyear.(df[!,:time])
    yearspresent=unique(df[!,:year])
    filteryear(yr)=df[df[!,:year].== yr,:]
    lists=filteryear.(yearspresent)
    lists=(xs->xs[!,Not(:year)]).(lists)
    totallength=min((xs->size(xs)[1]).(lists)...)
    trimmedlist=(xs->xs[collect(1:totallength),:]).(lists)
    arrays=Array.(trimmedlist)
    matrix = cat(arrays...,dims=3)
    rotatedmatrix=cat([matrix[:,i,:] for i in 1:size(matrix)[2]]...,dims=3)
    return rotatedmatrix
end